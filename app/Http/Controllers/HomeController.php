<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::where('user_id','=', Auth::id())->orderBy('created_at','desc')->get();
        $userid = Auth::user()->name;

        return view('home', compact('events','userid'));
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'heading' => 'required|max:40',
            'description' => 'required|max:1000'
        ]);


        $user = new Event;
        $user->title = Input::get('heading');
        $user->description = Input::get('description');
        $user->user_id = $request->user()->id;
        $user->save();
        return redirect('/home');
    }

}
