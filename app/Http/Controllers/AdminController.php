<?php

namespace App\Http\Controllers;

use App\Event;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $podaci =  DB::table('users')
        ->join('events', 'users.id', '=', 'events.user_id')
        ->select('users.id as uid', 'users.name as uname', 'events.*')
            ->orderBy('created_at','desc')
        ->get();
        $ime = Auth::user()->name;
        return view('admin', ['tasks' => $podaci, 'ime'=> $ime]);
    }
}
