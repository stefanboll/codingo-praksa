@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color:#f74747;opacity: 0.6;color:white;">USER Dashboard</div>

                    <div class="panel-body" style="background-color:#f20707;opacity: 0.6;color:white;">
                        You are logged in as <strong>user</strong> !
                    </div>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div>



    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color:#4b89ed;opacity: 0.6;color:white;"><strong>Dnevnik</strong>
                    </div>

                    <div class="panel-body" style="background-color:#045eef;opacity: 0.6;color:white;">
                        <form action="store" method="post">
                            <label for="heading"><strong>Naslov:</strong></label>
                            <input type="text" name="heading" id="heading"> <br/>
                            <label for="description"><strong>Opis:</strong></label>
                            <textarea name="description" id="description" style="width:100%"></textarea>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="submit" value="Submit" name="submit" id="submit" class="btn btn-danger">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach($events as $aktivnosti)
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color:#f74747;opacity: 0.6;color:white;"> <strong>Naslov:</strong>  {{ $aktivnosti->title }}
                    <div style="float: right;text-align: right;"><strong>
                            {{ $aktivnosti->created_at }}</strong>
                    </div>
                </div>
                <div class="panel-body" style="background-color:#f20707;opacity: 0.6;color:white;">
                    {{ $aktivnosti->description }}
                </div>
            </div>
        </div>
    @endforeach




@endsection