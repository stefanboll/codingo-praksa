@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"  style="background-color:#4b89ed;opacity: 0.6;color:white;">ADMIN Dashboard</div>

                <div class="panel-body" style="background-color:#045eef;opacity: 0.6;color:white;">
                    You are logged in as <strong>ADMIN</strong>!
                    <br>
                    Welcome <strong> {{$ime}} ! </strong>
                </div>
            </div>
        </div>
    </div>
</div>

@foreach($tasks as $task)
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color:#4b89ed;opacity: 0.6;color:white;"> <strong>Korisnik:</strong>  {{ $task->uname }}
                <div style="float: right;text-align: right;"><strong>
                        {{ $task->created_at }}</strong>
                </div>
            </div>
            <div class="panel-body" style="background-color:#045eef;opacity: 0.6;color:white;">
                <div class="col-md-4">
                <strong>Naslov:</strong> {{ $task->title }}
                </div>
                <div class="col-md-8">
                   <strong>Opis:</strong> {{ $task->description }}
                </div>
            </div>
        </div>
    </div>
@endforeach




@endsection
