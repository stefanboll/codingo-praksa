<?php

use Illuminate\Database\Seeder;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('project')->truncate();

        \DB::table('project')->insert([
        'username' => 'Stefan',
        'permissions' => 'admin',
        'email' => 'stefan@gmail.com'
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ])

        \DB::table('project')->insert([
            'username' => 'Ivan',
            'permissions' => 'user',
            'email' => 'ivan@gmail.com'
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        \DB::table('project')->insert([
            'username' => 'Lazar',
            'permissions' => 'user',
            'email' => 'lazar@gmail.com'
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
